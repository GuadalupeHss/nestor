﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daño_Jeringuilla : MonoBehaviour {
  

    GameObject player;
	public int dañoJeringuilla = 2;

   
   
	void Start()
	{
		player = GameObject.FindWithTag ("Player");
	}
    


	void Update () {
       
		
	}
 

	void OnCollisionEnter2D (Collision2D other)
	{
		


		if (other.gameObject.CompareTag ("Escenario")) {
			DestroyObject (gameObject);
		}


// He cambiado el codigo para que me fuese el sonido del daño.
		if(other.gameObject.CompareTag("Player"))
			{ 
			player.GetComponent<Player_Health>().TakeDamage(dañoJeringuilla);
			
			}
	}

void OnCollisionExit2D(Collision2D other)
{
	DestroyObject (gameObject);
}
}
