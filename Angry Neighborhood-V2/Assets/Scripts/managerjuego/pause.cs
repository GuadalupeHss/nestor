using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pause : MonoBehaviour {
    public bool active;
    public GameObject panel;  

    public bool paused = false;
    
    public Crono crono;
   


	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !paused) //si se presiona ESC saca el panel pausa
        {
            paused = true;
            ShowPanel(panel);
            Time.timeScale =  0;
            crono.Paused();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && paused)
        {
            paused = false;
            ShowPanel(panel);
            Time.timeScale =  1f;
            crono.Despausar();
        }
	}

        public void ShowPanel (GameObject panel) //muestra el panel
	{
        panel.SetActive (paused);
    }

        public void ClosePanel (GameObject panel) // esconde el panel
	{
        paused = false;
        ShowPanel(panel);
        Time.timeScale =  1f;
        crono.Despausar();
    }
}
