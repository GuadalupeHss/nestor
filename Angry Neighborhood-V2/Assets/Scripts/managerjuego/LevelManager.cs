﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
 
public class LevelManager : MonoBehaviour
{
 public bool CanChange = false; //true si matas al boss
 private bool goUp = false; //true si se acerca al objeto que da acceso a subir //
private bool showMessage = false;
 private GameManager gm;
 public Crono crono;


	// Use this for initialization
	void Start () 
	{
		gm = GameObject.FindObjectOfType<GameManager>();
		gm.setPlayerIsDead(false);
		gm.setPlayerWin(false);
		
	}
	 
	// Update is called once per frame
	void Update () 
	{	
		if(gm.playerIsDead == true)
		{
			SceneManager.LoadScene(6);
			crono.finished = true;
			gm.setPlayerIsDead(false);
			gm.setPlayerWin(false);
		}		

		if(gm.playerWin == true)
		{
			SceneManager.LoadScene(7);
			crono.finished = true;
			gm.setPlayerWin(false);
		}	
		
	


	//cambiar de nivel mientras funciona el trigger
		
		if (Input.GetKeyDown(KeyCode.Q)) 	//cambiar de nivel mientras finciona el trigger
			{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
			//float playtime = crono.playtime;
			//gm.setplaytime(playtime);
			//Debug.Log (playtime);
			}

	//si has matado al boos y se ha activado el trigger cambia la escena
	//	if(CanChange == true && goUp == true)//
	//		{
	//		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);//
	//		}
	//	}
	}

		//cuando entra el colision un objeto con la etiqueta Player con el trigger del objeto verifica  la 
		//condicion si ha matado al boos si es cierta cambia de nivel .	
	
	void OnTriggerEnter2D(Collider2D infoAcceso)
		{
		Debug.Log ("detecto triggerenter");
		if (infoAcceso.gameObject.tag == "Player" && gm.getDeadBoss()) 
			{
				goUp = true;//
				Debug.Log ("Player detectado");
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
			}
			else if (infoAcceso.gameObject.tag == "Player" && !gm.getDeadBoss()) 
			{
				goUp = false;//
				Debug.Log ("Player detectado"); 
				showMessage = true;
			}
		}

		

	private void OnGUI() 
	{
		if(showMessage)
		{
		GUIStyle myStyle = new GUIStyle();
		myStyle.fontSize = 60;
     	
		GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 300, 200), "You're not ready!", myStyle);

		}
	}

 
	void OnTriggerExit2D(Collider2D other)
		{
			Debug.Log ("detecto triggerexit");
			if (other.tag == "Player")
			{ 
				goUp = false;
				showMessage = false;
			}
		}
	}
