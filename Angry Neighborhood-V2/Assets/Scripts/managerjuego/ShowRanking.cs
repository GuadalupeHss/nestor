﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShowRanking : MonoBehaviour 
{
	public List<Text> textosNombres;
	public List<Text> textosTiempos;

	private int seconds;
	private int minutes;
	private int hours;
	private float t;


	private GameManager gm;


	public Serialized serialized; 
	
	// Use this for initialization
	void Start () {
		gm = GameObject.FindObjectOfType<GameManager>();
		serialized = GameObject.FindObjectOfType<Serialized>();
		//Panel_recordName.SetActive (true);
		serialized.Deserializa();

		// Pasamos la info de tiempos y nombres a la UI
		for	( int i=0; i<5; i++) {
			textosNombres[i].text = serialized.nombres[i];
			textosTiempos[i].text = "" + serialized.tiempos[i];

		
		}

	}
	void Update () {}


}
