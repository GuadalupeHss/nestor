﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Serialized : MonoBehaviour {


	public List<string> nombres;
	public List <float> tiempos;
	private FileStream file;
	private BinaryFormatter formatter;
	private string path;
	public int pos;
	public string nuevonombre;
	public float playtime;
	public bool madeRecord;
	private GameManager gm;

	// Inicializa el formateador y carga el fichero file.txt, o lo crea si no existe
	void Start () {
        formatter = new BinaryFormatter ( );
		path = Application.persistentDataPath + "/" + "ranking.txt";
		if ( !File.Exists ( path ) ) {
			file = File.Create ( path );
			file.Close ( );
			
		}
		else {
			file = File.OpenRead ( path );
			nombres = (List<string>) formatter.Deserialize ( file );
			tiempos = (List<float>)formatter.Deserialize (file);
			file.Close ( );
		}
		gm = GameObject.FindObjectOfType<GameManager>();


		
	}
	
	// Update is called once per frame
	void Update () {

		// SERIALIZA (guarda) contenido en el fichero
		if ( Input.GetKeyDown ( KeyCode.M ) ) {
			Serializa();
		}

		// DESERIALIZA (carga) contenido del fichero
		if ( Input.GetKeyDown ( KeyCode.B ) ) {
			Deserializa();
		}

		// INSERTA un elemento nuevo en la lista
		if ( Input.GetKeyDown ( KeyCode.N ) ) {
			Inserta ( );
		}

	}
	public void Serializa() 	// SERIALIZA (guarda) contenido en el fichero
	{
		float playtime = gm.getplaytime();//
		pos = ConsultaPosicionInsercion ( playtime );//
		string nuevonombre =gm.getnuevoNombre();//
		file = File.OpenWrite ( path );
		formatter.Serialize( file , nombres );
		formatter.Serialize( file , tiempos );
		file.Close ( );
	}

	public void Deserializa() // DESERIALIZA (carga) contenido del fichero
	{
		float playtime = gm.getplaytime();//
		pos = ConsultaPosicionInsercion ( playtime );//
		string nuevonombre =gm.getnuevoNombre();//
		file = File.OpenRead ( path );
		nombres = (List<string>) formatter.Deserialize ( file );
		tiempos = (List<float>) formatter.Deserialize ( file );
		file.Close ( );
	}


	public void Inserta( )  
	{
			int pos;
			string nuevonombre = gm.getnuevoNombre();
			Debug.Log("leo nuevo nombre");
			float playtime = gm.getplaytime();
			Debug.Log("tiempo de juego = "+playtime);
			pos = ConsultaPosicionInsercion ( playtime );
			Debug.Log(pos);
			if ( pos < 5 ) {
				nombres.Insert ( pos , nuevonombre);	
				tiempos.Insert ( pos , playtime );
				BorraUltimo ( );
				Serializa();
			}
	}
				
	public void Compara()
	{
			float playtime = gm.getplaytime();
			pos = ConsultaPosicionInsercion ( playtime );
			Debug.Log(pos);
			if ( pos < 5 )
			{
				madeRecord = true;
			}
	}


	public void BorraUltimo(){
		nombres.RemoveAt(5);
		tiempos.RemoveAt(5);
	}

	public int ConsultaPosicionInsercion ( float tiempo ) {
		int contador = 0;
		foreach ( float actual in tiempos ) {
			if ( actual > tiempo ) break;
			contador++;
		}
		return contador;
	}

}

