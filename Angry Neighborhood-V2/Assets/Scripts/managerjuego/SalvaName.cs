﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SalvaName : MonoBehaviour {
private GameManager gm;
public Serialized serialized;
public string nuevonombre;
public float playtime;

	// Use this for initialization
	void Start () 
	{
		gm = GameObject.FindObjectOfType<GameManager>();
		serialized = GameObject.FindObjectOfType<Serialized>();
	// Use this for initialization
        var input = gameObject.GetComponent<InputField>();
        var se= new InputField.SubmitEvent();
        se.AddListener(Salva);
        input.onEndEdit = se;
		
		
    }
    public void Salva (string arg0)
    {
        Debug.Log(arg0);
		print (arg0);
		gm.setnuevoNombre (arg0);
		//playtime = gm.getplaytime();
		//nuevonombre = gm.getnuevoNombre ();
		serialized.Inserta();
		serialized.Deserializa();
		SceneManager.LoadScene (9);
    }

	// Update is called once per frame
	void Update () 
	{
	}
		

	
}
