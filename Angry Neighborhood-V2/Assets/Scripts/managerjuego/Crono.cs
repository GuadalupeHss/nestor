﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crono : MonoBehaviour 
{

public Text timerText;
public float starttime;
public float playtime;// tiempo jugado
private int seconds;
private int minutes;
private int hours;
public bool paused = false;
public bool finished = false;
private GameManager gm;

	// Use this for initialization
	void Start () 
	{
		starttime = Time.time; //inicio
		gm = GameObject.FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (finished == true) Finished();
		
		if (paused == true ) return; // si no está pausado no sigas
			
		float t = Time.time - starttime + playtime; // t es el tiempo transcurrido menos el tiempo de inicio
		int tt =  Mathf.RoundToInt(t);
		seconds = (tt %60); //aumenta los segundos nunca superiora 60
		minutes = (tt /60) %60;//divide los segundos entre 60 para obtener los minutos nunca superiora 60
		hours = (tt /3600); //divide los segundos entre 3600 (segundo en 1 hora) para obtener los minutos
		
	}

	public void Paused () //cuando se detiene el tiempo elboleanoes cierto y el texto se congela en rojo
	{
	paused= true;
	playtime = Time.time - starttime + playtime;
	}

	public void Despausar() //cuando se espausa el tiempo el boleano e falso y se reinicia el tiempo
	{
	paused= false;
	starttime = Time.time;
	}

	public void Finished() //cuando se espausa el tiempo el boleano e falso y se reinicia el tiempo
	{
	//playtime = Time.time - starttime + playtime;
	gm.setplaytime(playtime); //NO LO SETEA
	timerText.color = Color.red;
	Debug.Log ( "FINALIZANDO" );
	gm.playerWin =true;
	}

	void OnGUI()// muestra el tiempo en el texto del canvas
	{
		timerText.text = hours.ToString() + " : " + minutes.ToString() + " : " + seconds.ToString();
	}

	void OnDestroy()  //
    {
		playtime = Time.time - starttime + playtime;
		print  ( playtime );
		gm.setplaytime ( playtime );// NO LO SETEA
    }
}
