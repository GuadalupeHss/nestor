﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpVida : MonoBehaviour {

	public GameObject jugador;
	public int PuntosRestauracionSalud = 50;

	void Start () {
		jugador = GameObject.FindGameObjectWithTag ("Player");
	}

	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D (Collision2D colision)
	{
		if (colision.gameObject.CompareTag("Player")){
		
			jugador.GetComponent<Player_Health> ().currentHealth = jugador.GetComponent<Player_Health> ().currentHealth + PuntosRestauracionSalud;
			Destroy (gameObject);
		}
	}
}

