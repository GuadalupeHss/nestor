﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpVelocidad : MonoBehaviour {

	public GameObject Jugador;
	public float velocidadExtra = 6;


	void Start () {
		Jugador = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D (Collision2D colision)
	{
		if (colision.gameObject.CompareTag("Player")){

			Jugador.GetComponent<Player_Controller> ().speed = Jugador.GetComponent<Player_Controller> ().speed + velocidadExtra;
			Destroy (gameObject);
		}
}
}