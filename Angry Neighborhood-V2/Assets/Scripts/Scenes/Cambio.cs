﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambio : MonoBehaviour {
public AudioClip clip;
public AudioSource audioSource;
public Animator anim;
public Animator animcamera;

	// Use this for initialization
	void Start () {
	anim= GetComponent<Animator>();	
	}
	
	// Update is called once per frame
	void Update () {

		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player")){
         audioSource.clip = clip;
		 audioSource.Play();
		 animcamera.SetBool("pasado",true);
		}
		
	}
	void OnTriggerExit2D(Collider2D other) {
		anim.SetTrigger("Pasar");
		anim.SetTrigger("hablar");
         
		
	}
}
