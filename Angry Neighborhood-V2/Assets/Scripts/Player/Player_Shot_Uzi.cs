﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Shot_Uzi : MonoBehaviour
{


    // Use this for initialization	public float maxSpeed = 5f;

    public GameObject mProyectileFire;
    public float mReloadFire;
    public float mProjectileSpeed;
    public bool disparo;
    public AudioClip Disparo_Sound;
    




    private float mLastShotTime = 0.0f;
    public AudioSource audioSource;



    // Use this for initialization
    void Start()
    {

        mLastShotTime = -mReloadFire;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire2") && this.GetComponentInParent<Player_Controller>().grounded == true)
        {
            shoot_Uzi();
        }
    }
    void shoot_Uzi()
    {
        this.GetComponentInParent<Animator>().SetTrigger("Shot");
        disparo = true;
        this.GetComponent<Animator>().SetBool("Disparando", disparo);
        audioSource.clip = Disparo_Sound;
        audioSource.Play();
        GameObject currentProjectile = Instantiate(mProyectileFire);

        if (this.GetComponentInParent<Player_Controller>().derecha == true && disparo == true)
        {
            currentProjectile.transform.position = transform.position + 2.0f * transform.right;
            currentProjectile.GetComponent<Rigidbody2D>().AddForce(transform.right * mProjectileSpeed);
        }

        if (this.GetComponentInParent<Player_Controller>().derecha == false && disparo == true)
        {
            currentProjectile.transform.localScale = new Vector2(-17.6953f, 11.35954f);
            currentProjectile.transform.position = transform.position + 2.0f * -transform.right;
            currentProjectile.GetComponent<Rigidbody2D>().AddForce(-transform.right * mProjectileSpeed);
        }
    }

}








