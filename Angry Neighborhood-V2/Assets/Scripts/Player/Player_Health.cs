﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Health : MonoBehaviour {
   
   
    public int startinghealth = 100;
    public  int currentHealth;
    public Player_Controller player_Controller;

    public AudioClip muerte_Sound;
    public AudioClip daño;
    
    
    private ParticleSystem particulas;
   
    private GameManager gm;
    private SpriteRenderer spr;
    
    Animator anim;
    AudioSource muerte;
    

    bool isDead;
    bool damaged;

	public GameObject barraSalud;
	public Scrollbar scrollbar;

	public float PorcentajeBarra;

    void Awake()
    {
       anim = GetComponent<Animator>();
       muerte = GetComponent<AudioSource>(); 
       currentHealth = startinghealth;
		barraSalud = GameObject.Find ("BarraSaludPlayer");

	}

    void Start() 
    {
		
        gm = GameObject.FindObjectOfType<GameManager>();
        currentHealth = gm.getPlayerHealth();
        particulas = GetComponentInChildren<ParticleSystem>();

    }

 
    void Update()
    {
     
        if (currentHealth <= 0 && !isDead)
        {
            anim.SetTrigger("Die");
            muerte.clip=muerte_Sound;
            muerte.Play();
            gm.playerIsDead=true;
            player_Controller.enabled = false;
        }
      
		PorcentajeBarra = currentHealth * 0.01f;
		barraSalud.GetComponent<Scrollbar> ().size = PorcentajeBarra;

		if (currentHealth > 100) {
			currentHealth = 100;
		}
    }

    
    public void TakeDamage(int amount)
    {
        damaged = true;
        currentHealth -= amount;
        muerte.clip=daño;
        muerte.Play();
        particulas.Play();
        
        if (currentHealth <= 0 && !isDead)
        {
            anim.SetTrigger("Die");
            muerte.clip=muerte_Sound;
            muerte.Play();
            gm.playerIsDead=true;
            player_Controller.enabled = false;
        }
    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDestroy()
    {
        gm.setPlayerHealth(currentHealth);
    }
    
    

}
