﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class EjempoSerialize : MonoBehaviour {

	public List<string> textos;
	public List<float> tiempos;
	private FileStream file;
	private BinaryFormatter formatter;
	private string path;
	public int pos;
	public string nuevonombre;
	public float playtime;
	 public GameManager gm;

	// Inicializa el formateador y carga el fichero file.txt, o lo crea si no existe
	void Start () {
        formatter = new BinaryFormatter ( );
		path = Application.persistentDataPath + "/" + "file.txt";
		if ( !File.Exists ( path ) ) {
			file = File.Create ( path );
			file.Close ( );
		}
		else {
			file = File.OpenRead ( path );
			textos = (List<string>) formatter.Deserialize ( file );
			file.Close ( );
		}
		gm = GameObject.FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		// SERIALIZA (guarda) contenido en el fichero
		if ( Input.GetKeyDown ( KeyCode.M ) ) {
			Serializa();
		}

		// DESERIALIZA (carga) contenido del fichero
		if ( Input.GetKeyDown ( KeyCode.B ) ) {
			Deserializa();
		}

		// INSERTA un elemento nuevo en la lista
		if ( Input.GetKeyDown ( KeyCode.N ) ) {
			Inserta(pos);
		}
	}

	// SERIALIZA (guarda) contenido en el fichero
	void Serializa()  {
		file = File.OpenWrite ( path );
		formatter.Serialize( file , textos );
		file.Close ( );
		}

	// DESERIALIZA (carga) contenido del fichero
	void Deserializa()  {
		file = File.OpenRead ( path );
		textos = (List<string>) formatter.Deserialize ( file );
		file.Close ( );
		}

	// INSERTA un elemento nuevo en la lista en una posición
	public void Inserta(int pos)  //¿NO HAY QUE PASAR PARÁMETROS? para leer la posición neceraris
	{
			pos = 5; // vendrá dadoCOMO LO LLAMO
			string nuevonombre = gm.getnuevoNombre();
			float playtime = gm.getplaytime();
			textos.Insert ( pos , nuevonombre); // 		
			tiempos.Insert (pos,playtime);
		}

	public void BorraUltimo(){
		textos.RemoveAt(6);
		tiempos.RemoveAt(6);
	}
}
