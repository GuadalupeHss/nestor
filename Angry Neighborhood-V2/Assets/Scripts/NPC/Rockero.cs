﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Rockero : MonoBehaviour {


	public SpriteRenderer enemigo;
	public float salud = 10;
	public float dañoRecibe=1;
	public GameObject Arma;
	public Transform SalidaArmaIzquierda;
	public Transform SalidaArmaDerecha;
	public GameObject dondeMira;
	public bool activarDisparo = false;
	public GameObject ArmaActual;
	public int munición = 0;
	public float cooldown = 0f;
	public float cooldownMax =0.7f;


	void Update (){
			
		cooldown = cooldown - Time.deltaTime;

		if (cooldown <= 0) {
			
			StartCoroutine ("tiempoEntreDisparo");
			cooldown = cooldownMax;
			}

	}

	// funcion para crear las balas y lanzarlas
	void disparoDerecha(){
		if (enemigo.GetComponent<SpriteRenderer> ().flipX == false ) {
			GameObject ArmaActual = Instantiate (Arma);
			ArmaActual.transform.position = SalidaArmaDerecha.transform.position;
			ArmaActual.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (1, 1) * 10000 * Time.deltaTime);

		}
	}
		void disparoIzquierda(){
		if (enemigo.GetComponent<SpriteRenderer> ().flipX == true ) {
				GameObject ArmaActual = Instantiate (Arma);
				ArmaActual.transform.position = SalidaArmaIzquierda.transform.position;
				ArmaActual.GetComponent<Rigidbody2D> ().AddForce (new Vector2(-1,-1) * 10000 * Time.deltaTime);
							}


	}


	IEnumerator tiempoEntreDisparo (){
		yield return new WaitForSeconds (2);
		enemigo.GetComponent<SpriteRenderer> ().flipX = false;
		activarDisparo = true;

		disparoDerecha ();
		activarDisparo = false;
		yield return new WaitForSeconds (2);
		enemigo.GetComponent<SpriteRenderer> ().flipX = true;
		activarDisparo = true;
		disparoIzquierda ();
		activarDisparo = false;
	}


	void OnCollisionEnter2D (Collision2D other){

		if (other.gameObject.CompareTag("Player")) {
			salud = salud - dañoRecibe;
			Debug.Log ("quitale daño al rockero");
		}


		}
}
	