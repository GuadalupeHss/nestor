﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {
public int vida=100;
public bool isDead;
public int vidaActual;
public Animator anim;


	// Use this for initialization
	void Awake () {
	vidaActual=vida;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	 public void TakeDamage(int amount)
    {
        
        vidaActual -= amount;
        
        
        if (vidaActual <= 0 && !isDead)
        {
            anim.SetBool("Muerto",true);
           // muerte.clip=muerte_Sound;
            //muerte.Play();
            //gm.playerIsDead=true;
           isDead=true;
        }
    }
}

