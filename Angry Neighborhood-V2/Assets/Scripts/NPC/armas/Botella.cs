﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botella : MonoBehaviour {
  

    GameObject player;
	public int dañoBotella = 5;
	public Sprite botellaRota;
	public SpriteRenderer botella;
   
	void Start()
	{
		botella = gameObject.GetComponent<SpriteRenderer> ();
		player = GameObject.FindWithTag ("Player");
	}
    


	void Update () {
       
		
	}
 

	void OnCollisionEnter2D (Collision2D other)
	{
		


		if (other.gameObject.CompareTag ("Escenario")) {
			botella.sprite = botellaRota;
			Debug.Log ("da tiempo a cambiar");
			StartCoroutine ("destruir");
		}



		if(other.gameObject.CompareTag("Player"))
			{ 
			player.GetComponent<Player_Health>().currentHealth = player.GetComponent<Player_Health>().currentHealth - dañoBotella;
			botella.sprite = botellaRota;
			Debug.Log ("da tiempo a cambiar");
			StartCoroutine ("destruir");
			}

		if (other.gameObject.CompareTag ("Enemy")) {
			botella.sprite = botellaRota;
			Debug.Log ("da tiempo a cambiar");
			StartCoroutine ("destruir");
		}

	}


	IEnumerator destruir(){
		yield return new WaitForSeconds (0.4f);
		DestroyObject (gameObject);

	}
}
