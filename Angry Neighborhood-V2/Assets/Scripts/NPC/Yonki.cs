﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Yonki : MonoBehaviour {

	public float dañoRecibe = 1;
	public float salud = 10;
	public Transform posiciónIzq;
	public Transform posiciónDch;
	public float velocidad;
	public bool andarDerecha;
	public SpriteRenderer enemigo;
	public CapsuleCollider2D colliderDistancia;
	public GameObject padre;
	public GameObject ArmaDistancia;
	public Transform SalidaArmaIzquierda;
	public Transform SalidaArmaDerecha;
	public GameObject dondeMira;

	public float cooldown = 0f;
	public float cooldownMax =1f;

	public bool atacar;
	public Animator animación;
	public Animator anim;
    private GameManager gm;

  void Start(){
	  gm = GameObject.FindObjectOfType<GameManager>();
  }

	void Update (){
			
		if (andarDerecha && (this.transform.position.x <= posiciónDch.position.x) && atacar==false) {
			transform.position = Vector3.MoveTowards (this.transform.position, posiciónDch.position, velocidad * Time.deltaTime);

		}

		if (andarDerecha && (this.transform.position.x >= posiciónDch.position.x) && atacar==false) {
			andarDerecha = false;
			enemigo.flipX = true;
			colliderDistancia.offset = new Vector2 (-6.5f, 0.53f);



		} else if (!andarDerecha && (this.transform.position.x > posiciónIzq.position.x) && atacar==false) {
			transform.position = Vector3.MoveTowards (this.transform.position, posiciónIzq.position, velocidad * Time.deltaTime);
	
		} else if (!andarDerecha && (this.transform.position.x <= posiciónIzq.position.x) && atacar==false) {
			andarDerecha = true;
			enemigo.flipX = false;
			colliderDistancia.offset = new Vector2 (6.5f, 0.53f);


		}


		if (salud <= 0) {
			
			anim.SetBool("Enemigo_muerto",true);
			gm.deadBoss=true;
			DestroyObject (padre);
		}

	}

	//Una vez detecte el collider de otro, activa atacar para que deje de moverse, y activa el Ienumerator.
	void OnTriggerStay2D(Collider2D other)
	{

		if (other.gameObject.CompareTag ("Player")) {
			atacar = true;
			cooldown = cooldown - Time.deltaTime;

			if (cooldown <= 0f) {
				disparo ();
				cooldown = cooldownMax;
			}
		}
	}

	// Cuando sale del collider desactiva atacar para seguir moviendose.
	void OnTriggerExit2D(Collider2D other){
		atacar = false;
		animación.SetBool ("Ataque", false);
		}


	// funcion para crear las balas y lanzarlas
	void disparo(){
		if (!andarDerecha) {
			ArmaDistancia.GetComponent<SpriteRenderer> ().flipY = false;
			GameObject ArmaActual = Instantiate (ArmaDistancia);
			ArmaActual.transform.position = SalidaArmaIzquierda.transform.position;
			ArmaActual.GetComponent<Rigidbody2D> ().AddForce (Vector2.left * 10000 * Time.deltaTime);
			animación.SetBool ("Ataque", true);
		}

		if (andarDerecha) {
						ArmaDistancia.GetComponent<SpriteRenderer> ().flipY = true;
			GameObject ArmaActual = Instantiate (ArmaDistancia);
			ArmaActual.transform.position = SalidaArmaDerecha.transform.position;
			ArmaActual.GetComponent<Rigidbody2D> ().AddForce (Vector2.right * 50000 * Time.deltaTime);
			animación.SetBool ("Ataque", true);
			ArmaDistancia.GetComponent<SpriteRenderer> ().flipY = true;
		}
			}




	void OnCollisionEnter2D (Collision2D other){

		if (other.gameObject.CompareTag ("Player")) {


			if (other.gameObject.GetComponent<Player_Controller> ().atacando == true) {
				salud = salud - dañoRecibe;

			}

		}


	}


}
	