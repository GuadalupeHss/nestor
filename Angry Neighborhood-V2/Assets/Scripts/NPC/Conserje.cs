﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Conserje : MonoBehaviour 
{

	Rigidbody2D rigidbodyPowerUp;

	public bool activarDar = true;

	public Animator animación;


	public GameObject[] powerups = new GameObject[2];
	public GameObject powerUpAleatorio;


	//Una vez detecte el collider de otro,verifica que es player y le da el objeto al azar.
	void OnTriggerEnter2D(Collider2D infoAcceso)
	{
		if (infoAcceso.gameObject.tag == "Player") 
		{
			powerUpAleatorio = powerups [Random.Range (0, 2)];

			if (activarDar == true) {

				animación.SetBool ("dar", true);
				powerUpAleatorio.SetActive (true);
				rigidbodyPowerUp = powerUpAleatorio.GetComponent<Rigidbody2D> ();
				rigidbodyPowerUp.AddForce (new Vector3 (-5,5), ForceMode2D.Impulse);	

			}
		}

	}


	void OnTriggerExit2D(Collider2D other){
		activarDar= false;
		animación.SetBool ("dar", false);
		}

		
}
		
	