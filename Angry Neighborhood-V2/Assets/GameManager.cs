﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour 
{

	public bool deadBoss; //condicion si el boos está muerto
	public bool playerIsDead; //condicion si el player está muerto
	public bool playerWin; //condicion si el player gana
	public int playerHealth = 100; // vida quequeda
	public float playtime; //tiempojugado
	public string nuevoNombre;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {}


	public void setDeadBoss( bool _bossdead)
	{
		deadBoss = _bossdead;
	}

	public bool getDeadBoss()
	{
		return deadBoss;
	}

	public void setPlayerIsDead( bool _playerIsDead)
	{
		playerIsDead = _playerIsDead;
	}

	public bool getPlayerIsDead()
	{
		return playerIsDead;
	}

	public void setPlayerWin( bool _playerWin)
	{
		playerWin = _playerWin;
	}

	public bool getPlayerWin()
	{
		return playerWin;
	}
	public void setPlayerHealth( int _health)
	{
		playerHealth = _health;
	}

	public int getPlayerHealth()
	{
		return playerHealth;
	}
	public void setplaytime( float _playtime)
	{
		playtime = _playtime;
	}

	public float getplaytime()
	{
		//int playtimet =  Mathf.RoundToInt(playtime);
		return playtime;
	}

	public void setnuevoNombre( string _nuevoNombre)
	{
		nuevoNombre = _nuevoNombre;
	}

	public string getnuevoNombre()
	{
		return nuevoNombre;
	}
}
